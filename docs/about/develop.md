---
lang: zh-cn
title: 参与项目
---

# 参与项目

无论是普通用户还是开发者，我们都非常欢迎你参与海豹的开发，一起努力让海豹变得更好。

海豹的所有源代码都托管在 [海豹开源组织 GitHub](https://github.com/sealdice) 下，每个模块都存放在对应的仓库中，可阅读对应仓库的 Readme 获取更多信息。

几个比较重要的仓库：
- [核心](https://github.com/sealdice/sealdice-core)：海豹的 Go 后端代码仓库，这个仓库也作为海豹的主仓库，Bug 可反馈在该仓库的 issue 中；
- [UI](https://github.com/sealdice/sealdice-ui)：海豹的前端代码，基于 Vue3 + ElementPlus 开发；
- [手册（新）](https://github.com/sealdice/sealdice-manual-next)：海豹新版官方手册（即当前的手册）的源码，如对手册有什么改进内容可以在该项目提交 pr；
- [构建](https://github.com/sealdice/sealdice-build)：海豹的自动构建仓库，用于自动化发布海豹的每日构建包与 Release；
- ……

## 如何参与项目？

### Bug 反馈、功能建议

所有的 Bug 和功能建议都可反馈在 [核心](https://github.com/sealdice/sealdice-core) 仓库的 issue 中，按照模板填写可方便开发组快速定位问题。

::: tip 无法访问 GitHub？

受限于各种原因，不是所有人都能顺利访问 GitHub，如有 Bug 可以加入官方群进行反馈。但如果有条件，我们还是建议在 GitHub 向开发组反馈问题。

:::

### 提交修改

欢迎各种类型的 pr 提交，可以帮助改进代码，增加功能，也可以是完善文档等等。向对应仓库发起 pr 即可。
